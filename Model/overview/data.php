<?php

$mac = exec('cat /sys/class/net/usb0/address');

$connection = exec('sh /usr/bin/check_connection.sh');
switch ($connection) {
	case "Online":
		$connection_level = "good";
		break;
	case "Offline":
		$connection_level = "bad";
		break;
	default: 
		$connection = "N\A";
		$connection_level = "undefined";
}

$interface = exec("ls /sys/class/net | grep eth0");
switch ($interface) {
	case "eth0":
		$interface = "Ethernet";
		$interface_level = "good";
		break;
	case "eth1":
		$interface = "4G";
		$interface_level = "bad";
		break;
	default:
		$interface = "N\A";
		$interface_level = "undefined";
}

$signal = exec("sudo iwconfig");
switch ($signal) {
	default: 
		$signal = "N\A";
		$signal_level = "undefined";
}

$forwarder = exec("systemctl show -p SubState --value lora_gateway");
switch ($forwarder) {
	case "running":
		$forwarder_level = "good";
		break;
	case "failed":
		$forwarder_level = "bad";
		break;
	default:
		$forwarder = "N\A";
		$forwarder_level = "undefined";
}

$status = array(
	"connection" => array(
		"value" => $connection,
		"level" => $connection_level),
	"interface" => array(
		"value" => $interface,
		"level" => $interface_level),
	"signal" => array(
		"value" => $signal,
		"level" => $signal_level),
	"forwarder" => array(
		"value" => $forwarder,
		"level" => $forwarder_level)
);

$route = array();
exec("netstat -rn", $route);

