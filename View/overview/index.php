<!DOCTYPE html>
<html lang="en">
<head>
    <title>Overview</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="View/global/style.css">
    <link rel="stylesheet" href="View/overview/style.css">
</head>
<body>
<?php include __DIR__ . "/../global/menu.php"; ?>
<section id="content-wrapper">
    <h1>Overview</h1>
    <div id="services">
	<?php 
	foreach ($status as $service_name=>$service_status) {
	?>
	    <div class=<?= $service_status["level"]; ?>>
	        <h3><?= $service_name; ?></h3>
		<p><?= $service_status["value"]; ?></p>
	    </div>	
	<?php
	}
	?>
   </div>

    <h2>Routes</h2>
    <table>
    <?php
	array_shift($route);
	$i = 0;
        foreach ($route as $line){
                $array = explode(" ", $line);
                echo "<tr>";
		for ($j = 0; $j < count($array); ++$j){
			$element = $array[$j];
			if ($element != "" ){
				if ($i == 0){
					echo "<th>$element</th>";
				} else {
					echo "<td>$element</td>";
				}
                        }
                }
		echo "</tr>";
		$i++;
        }
?>
    </table>
</section>
</body>
</html>

	
